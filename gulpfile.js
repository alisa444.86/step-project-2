const gulp = require('gulp');
const sass = require('gulp-sass');
const clean = require('gulp-clean');
const autoPrefixer = require('gulp-autoprefixer');
const cleanCss = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const imageMin = require('gulp-imagemin');
const newer = require('gulp-newer');
const remember = require('gulp-remember');
const cached = require('gulp-cached');
const path = require('path');
const gulpIf = require('gulp-if');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const rigger = require('gulp-rigger');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

const cleanDist = () => {
    return gulp.src('dist/{css,js}', { read: false, allowEmpty: true })
        .pipe(clean());
};

const cssBuild = () => {
    return gulp.src('src/scss/reset.css')
        .pipe(gulp.dest('dist/css'));
};

const scssBuild = () => {
    return gulp.src('src/scss/*.scss') // , { since: gulp.lastRun(scssBuild) }
        .pipe(cached('scssBuild'))
        .pipe(remember('previouslyBuiltScssFiles'))
        .pipe(gulpIf (!isDevelopment, sourcemaps.init()) )
        .pipe(sass())
        .pipe(autoPrefixer())
        .pipe(gulpIf (!isDevelopment, cleanCss({ compatibility: 'ie10' })))
        .pipe(gulpIf (!isDevelopment, sourcemaps.write()))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
};

const distFonts = () => {
    return gulp.src('src/webfonts/*')
        .pipe(gulp.dest('dist/webfonts/'));
};

const jsBuild = () => {
    return gulp.src('src/js/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('dist/js/'));
};

const imgOptimization = () => {
    return gulp.src('src/img/**/*.*', { since: gulp.lastRun(imgOptimization) })
        .pipe(newer('dist/img'))
        .pipe(imageMin())
        .pipe(gulp.dest('dist/img'));
};

const distJQuery = () => {
    return gulp.src('./node_modules/jquery/dist/jquery.min.js')
        .pipe(gulp.dest('dist/js/'));
};

const concatHtml = () => {
    return gulp.src('src/html/index.html')
        .pipe(rigger())
        .pipe(gulp.dest('../step-project-2'));
};

const distBootstrap = () => {
    return gulp.src('./node_modules/bootstrap/dist/js/bootstrap.bundle.min.js')
        .pipe(gulp.dest('dist/js/'));
};

const distSlick = () => {
    return gulp.src('./src/slick/*')
        .pipe(gulp.dest('dist/slick/'))
}

const watch = () => {
    gulp.watch('src/scss/**/*.scss', gulp.parallel(cssBuild,scssBuild))
        .on('unlink', (filePath) => {
            remember.forget('previouslyBuiltScssFiles', path.resolve(filePath));
            delete cached.caches.styles[path.resolve(filePath)];
        });
    gulp.watch('src/img/**/*.*').on('change', browserSync.reload);
    gulp.watch('src/js/*.js', jsBuild).on('change', browserSync.reload);
    gulp.watch('src/html/**/*.html').on('change', gulp.series(concatHtml, browserSync.reload));
    browserSync.init({
        server: {
            baseDir: './'
        },
    });
};

gulp.task('build',
    gulp.series(cleanDist, gulp.parallel(
                                        concatHtml,
                                        distFonts,
                                        cssBuild,
                                        scssBuild,
                                        jsBuild,
                                        imgOptimization,
                                        distJQuery,
                                        distBootstrap,
                                        distSlick
                                        )));

gulp.task('watch', watch);
gulp.task('dev', gulp.series('build', 'watch'));