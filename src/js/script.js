/**------------load-more button-------------*/

const loadBtn = document.querySelector('.load-more');
let counter = 0;
let counterItems = 0;
let i = 0;
let finalPoint;

function openCards() {
    const screenWidth = document.documentElement.clientWidth;
    const furnitureItems = document.querySelectorAll('.card-box');
    if (screenWidth > 991) {
        furnitureItems.forEach(item => {
            if (item.classList.contains('d-none')) {
                item.classList.remove('d-none');
                loadBtn.classList.add('d-none');
            }
        })
    }
    if (screenWidth > 740 && screenWidth < 992) {
        if (counter === 0) {
            finalPoint = i + 8;
        } else {
            finalPoint = i + 4;
        }
        if (counter < 3) {
            for (i; i < finalPoint; i++) {
                if (i < finalPoint && furnitureItems[i].classList.contains('d-none')) {
                    furnitureItems[i].classList.remove('d-none');
                    counterItems++;
                }
            }
            counter++;
            if (counter >= 3) {
                loadBtn.classList.add('d-none');
            }
        }
    }

    if (screenWidth < 740) {
        if (counter === 0) {
            finalPoint = i + 4;
        } else {
            finalPoint = i + 2;
        }
        if (counter < 8) {
            for (i; i < finalPoint; i++) {
                if (i < finalPoint && furnitureItems[i].classList.contains('d-none')) {
                    furnitureItems[i].classList.remove('d-none');
                    counterItems++;
                }
            }
            counter++;
            if (counter === 8) {
                loadBtn.classList.add('d-none');
            }
        }
    }
}

loadBtn.addEventListener('click', () => {
    loadBtn.classList.add('rotate');
    setTimeout(() => {
        openCards();
        loadBtn.classList.remove('rotate');
    }, 2000);
});

/**---------filter-range--------------*/

$(function () {
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 500,
        values: [75, 300],
        slide: function (event, ui) {
            $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
        }
    });
    $("#amount").val("$" + $("#slider-range").slider("values", 0) +
        " - $" + $("#slider-range").slider("values", 1));
});

$(function () {
    $("#m-slider-range").slider({
        range: true,
        min: 0,
        max: 500,
        values: [75, 300],
        slide: function (event, ui) {
            $("#m-amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
        }
    });
    $("#m-amount").val("$" + $("#m-slider-range").slider("values", 0) +
        " - $" + $("#m-slider-range").slider("values", 1));
});


/**-------------filter__item active---------------*/
const filterItems = document.querySelectorAll('.filter__item');

filterItems.forEach(item => {

    item.addEventListener('click', (event) => {
        const target = event.target;
        if (target.tagName.toLowerCase() !== 'input') {
            item.classList.toggle('active');
        }

        if (target.classList.contains('rounded-circle')) {
            item.querySelector('input').click();
        }
    })
});

$(document).ready(function () {
    $(".filters__btn").click(function () {
        $("#filters-modal").modal('show');
    });
});

/**------------------product-details------------*/

const productsCollection = document.querySelectorAll('.furniture__item');

const productDetails = [
    {
        imgSrc: 'dist/img/products/furniture/1.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: false,
        price: '30.00',
        dataNumber: 0,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/2.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: false,
        price: '15.00',
        dataNumber: 1,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/3.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: true,
        lastPrice: '35.00',
        price: '25.00',
        dataNumber: 2,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/4.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: false,
        price: '15.00',
        dataNumber: 3,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/5.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: false,
        price: '15.00',
        dataNumber: 4,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/6.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: true,
        lastPrice: '35.00',
        price: '25.00',
        dataNumber: 5,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/7.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: false,
        price: '15.00',
        dataNumber: 6,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/8.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: false,
        price: '15.00',
        dataNumber: 7,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/9.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: true,
        lastPrice: '35.00',
        price: '25.00',
        dataNumber: 8,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/10.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: false,
        price: '30.00',
        dataNumber: 9,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/11.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: false,
        price: '15.00',
        dataNumber: 10,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/12.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: true,
        lastPrice: '35.00',
        price: '25.00',
        dataNumber: 11,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/13.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: false,
        price: '15.00',
        dataNumber: 12,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/14.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: false,
        price: '15.00',
        dataNumber: 13,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/15.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: true,
        lastPrice: '35.00',
        price: '25.00',
        dataNumber: 14,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/16.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: false,
        price: '15.00',
        dataNumber: 15,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/17.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: false,
        price: '15.00',
        dataNumber: 16,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/18.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: true,
        lastPrice: '35.00',
        price: '25.00',
        dataNumber: 17,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/19.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: true,
        lastPrice: '35.00',
        price: '25.00',
        dataNumber: 19,
        inCart: 0,
    },
    {
        imgSrc: 'dist/img/products/furniture/20.png',
        title: 'Aenean Ru Bristique',
        rate: 2,
        isSaling: true,
        lastPrice: '35.00',
        price: '25.00',
        dataNumber: 19,
        inCart: 0,
    }
];

const quickViewBtns = document.querySelectorAll('.btn-quick-view');
const furnitureBox = document.querySelector('.furniture');
const productModal = document.querySelector('#product-details');
const addBtns = furnitureBox.querySelectorAll('.btn-add-to-cart');
const addBtnModal = productModal.querySelector('.btn-add-to-cart');

for (let i = 0; i < productsCollection.length; i++) {
    productsCollection[i].setAttribute('data-number', `${i}`);
    quickViewBtns[i].setAttribute('data-number', `${i}`);
    addBtns[i].setAttribute('data-number', `${i}`);
}

addBtns.forEach(item => {
    item.addEventListener('click', () => {
        const dataAttr = item.getAttribute('data-number');
        addToCart(dataAttr);
        changeCartCounter();
    })
});

addBtnModal.addEventListener('click', () => {
    const dataAttr = productModal.getAttribute('data-number');
    addToCart(dataAttr);
    changeCartCounter();
});

function addToCart(dataAttr) {
    let productFromDB = Object.assign({},
        productDetails.find(product => product.dataNumber === Number(dataAttr)));
    let cartItems = JSON.parse(localStorage.getItem('cartItems'));

    cartItems = (cartItems && cartItems.length) ? cartItems : [];
    productFromDBinCart = cartItems.find(product => product.dataNumber === productFromDB.dataNumber);

    if (productFromDBinCart) {
        productFromDBinCart.inCart += 1;
    } else {
        productFromDB.inCart += 1;
        cartItems.push(productFromDB);
    }

    localStorage.setItem('cartItems', JSON.stringify(cartItems));
}

quickViewBtns.forEach(item => {
    item.addEventListener('click', (event) => {
        const dataAttr = item.getAttribute('data-number');

        productDetails.forEach(object => {
            if (object.dataNumber === Number(dataAttr)) {

                const src = object.imgSrc;
                const sale = object.isSaling;
                productModal.setAttribute('data-number', `${dataAttr}`);
                const modalSale = productModal.querySelector('.footer__sale');
                if (sale) {
                    modalSale.innerText = `$${object.lastPrice}`;
                } else {
                    modalSale.classList.add('d-none');
                }
                const modalImg = productModal.querySelector('.details__img');
                const modalPrice = productModal.querySelector('.footer__price');
                modalImg.setAttribute('src', `${src}`);
                modalPrice.innerText = `$ ${object.price}`;
            }
        });
        $("#product-details").modal('show');
    });
});

function changeInputPlaceholder() {
    const screenWidth = document.documentElement.clientWidth;
    if (screenWidth < 767) {
        const inputEmail = document.querySelector('#inputEmail');
        inputEmail.setAttribute('placeholder', 'Subscribe to newsletter');
    } else {
        inputEmail.setAttribute('placeholder', 'Enter your email address');
    }
}


$('[data-toggle="popover"]').popover().click(function () {
    setTimeout(function () {
        $('[data-toggle="popover"]').popover('hide');
    }, 800);
});


$(document).ready(
    changeInputPlaceholder()
);
